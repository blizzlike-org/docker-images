# buildenvironment

## travis

the travis workflow script requires the following ENV variables

| variable     | default    |
| ------------ | ---------- |
| DISTRONAME   | stretch    |
| GITHUB_TOKEN | -          |
| S3_ID        | -          |
| S3_KEY       | -          |
| TRAVIS_TOKEN | -          |

and the whole travis environment variables documented [here](https://docs.travis-ci.com/user/environment-variables/).
